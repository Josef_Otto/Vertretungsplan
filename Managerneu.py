from Vertretungsplanabrufen import *
import json
import os

def personmachen():
    adresse = input('Lernsax-E-Mail-Adresse einer Person\n\
(nachname.vorname@mcg-dresden.lernsax.de): ')
    klassen = []
    oberstufe = input('Falls die Person die Oberstufe besucht - 11 oder 12? (Wenn nicht frei lassen) ')
    if oberstufe:
        print('Das Folgende gilt nun für Kursnamen.')
    klasse = input('In welcher Klasse ist die Person (Der Klassenname \
muss so im Vertretungsplan vorkommen)? ')
    while True:
        eing = input('Weitere Kurse - frei lassen zum Beenden: ')
        if eing == '':
            break
        klassen.append(eing)

    immersenden = input('Soll der Vertretungsplan auch gesendet werden, wenn es keine Änderungen gibt? ').lower() in ('j', 'ja', 'y', 'yes')

    if oberstufe:
        return Person.Oberstufe(adresse, klasse, klassen, oberstufe, immersenden)
    
    return Person.Unterstufe(adresse, klasse, klassen, immersenden)

'''def personmachen___():
    klassen = []
    # z - Zustand, e - Eingabe, f - Frage
    z = 'email'
    e = None
    while True:
        match (e, z):
            case (_, 'email'):
                f = 'Lernsax-E-Mail-Adresse einer Person\n\
(nachname.vorname@mcg-dresden.lernsax.de): '
                z = 'hauptklasse'
            case (e, 'hauptklasse'):
                adresse = e
                f = 'Weitere Klasse (z. B. der Religionskurs) - \
frei lassen zum Beenden: '
                if e[0].isnumeric():
                    if (kl := e[:2]).isnumeric():
                        if int(kl) > 10:
                            z = 'oberstufe'
                            
                z = nebenklassen

        e = input(f)'''

def personspeichern(person):
    '''Gibt den Dateipfad zurück.'''
    with open(f'Menschen/{person.vorname}_{person.nachname}.js', 'w') as datei:
        json.dump(person.alsliste(), datei)
    return datei.name


def zucrontab(dateipfad, stunde, minute=0):
    tage = '0,1,2,3,4' if stunde >= 10 else '1,2,3,4,5'
    os.system(f'(crontab -l ; echo "{minute} {stunde} * * {tage} python {os.path.dirname(__file__)}/Vertretungsplanabrufen.py {dateipfad}") | crontab')
    
if __name__ == '__main__a':
    nikolai = Person('marlow.nikolai@mcg-dresden.lernsax.de',
                     ['11/ ch2', '11/ PH1', '11/ ge4', '11/ MA3', '11/ fr1',
                      '11/ mu1', '11/ de3', '11/ geo1', '11/ inf2', '11/ spo5',
                      '11/ eth2', '11/ en21', 'GTA/ Chor', 'GTA/ MAspezi'])

if __name__ == '__main__':
    person = personmachen()
    print(person)
    print('Alle Klassen:', person.klassen)
    input('Bestätigen...')
    pfad = personspeichern(person)
    print(pfad)
    stunde = input('In welcher Stunde soll der Vertretungsplan verschickt werden? (0-23) ')
    minute = input('Minute: ')
    zucrontab(pfad, int(stunde), int(minute))
