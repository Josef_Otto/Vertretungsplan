import sys
from Vertretungsplanabrufen import *
import json

print(sys.argv)
daten = json.load(open(sys.argv[1]))
person = Person(*daten)

internetseite = bsseite('https://mcg-dresden.de/vertretung/')
datum = internetseite.find(name='span', attrs={'class': 'vpfuer'}).contents[1].contents[0]

vp = vertretungsplanabrufen(internetseite)

person.vpanpersonwennnötig(vp, datum)
