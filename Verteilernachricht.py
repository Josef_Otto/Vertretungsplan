import os
import json
from pprint import pprint
import quickmessage
import Vertretungsplanabrufen as VP

ordner = 'Menschen'

adressen = [json.load(open(ordner + '/' + mensch))[0] for mensch in os.listdir(ordner)]

pprint(adressen)

nachricht = input('Nachricht an alle ("VP" für Vertretungsplan): ')


with open('passwort.txt', 'r') as pw:
    pw = pw.read()

def verteilernachricht(empfänger_innenadressen: list, text):
    for i in empfänger_innenadressen:
        quickmessage.sendequickmessage(i, nachricht, 'marlow.nikolai@mcg-dresden.lernsax.de', pw)
        print('Nachricht an', i, 'versendet.')

def VPanalle():
    VP.vorbereiten()
    for name in os.listdir(ordner):
        daten = json.load(open(f'Menschen/{name}'))
        person = VP.Person(*daten)
        print(person)
        person.vpanpersonwennnötig(VP.vp)

        
if nachricht in ('VP', '"VP"'):
    input('heutigen Vertretungsplan an alle senden...')
    VPanalle()

else:
    print(nachricht)
    input('Bestätigen...')
    verteilernachricht(adressen, nachricht)
