from sys import path
path.append('..')
from Vertretungsplanabrufen import *
import pickle

person = pickle.loads({})  # Das wurde maschinell eingefügt.

internetseite = bsseite('https://mcg-dresden.de/vertretung/')
datum = internetseite.find(name='span', attrs={{'class': 'vpfuer'}}).contents[1].contents[0]

vp = vertretungsplanabrufen(internetseite)

person.vpanpersonwennnötig(vp, datum)
