2025-02-07T16:00:00

Nikolai Marlow

Komm zum Klimastreik am 7.2.!
Zwei Wochen vor der Bundestagswahl gehen wir auf die Straße, damit der Klimawandel nicht in den Hintergrund gedrängt wird! 2024 war das wärmste Jahr in Deutschland seit Beginn der Wetteraufzeichnungen. Die weltweite Durchschnittstemperatur ist erstmals auf über 1,5° gestiegen.
Demonstrieren wir gemeinsam für eine lebenswerte Zukunft, denn der Klimawandel bleibt das wichtigste Thema unserer Zeit.
In Dresden soll im ÖPNV gekürzt werden, verteidigen wir auch die Mobilitätswende.
Morgen (07.02.) um 15:00 am Neumarkt
