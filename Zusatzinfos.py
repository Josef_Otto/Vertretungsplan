import re
import regex
from os.path import commonprefix
#from typing import Union

__doc__ = '''Ermittelt die Adressatin / den Adressaten einer zusätzlichen Information
des Vertretungsplanes. Dazu kann nicht einfach geprüft werden, ob sich z. B.
der Klassenname einer Person darin befindet, weil dadurch für alle relevante
Infos abhanden kämen. Stattdessen hilft die Erkenntnis weiter, dass mögliche
Adressaten keine unendliche Menge sind: Eine Info richtet sich meist entwe-
der an
- eine oder mehrere Klassen oder
- an Klassenstufen oder
- an eine bestimmte Person oder
- an eine AG.
Die Adressaten dieser Kategorien werden mittels regulärer Ausdrücke ermit-
telt. Stimmt die Info mit einer Person überein, wird ihr die Info zugeord-
net. Konnten gar keine Adressaten ermittelt werden, könnte die Info die Per-
son möglicherweise betreffen, sollte als auch dann versendet werden.'''

klassennummer = r'[01]?\d'

def klassenformate(klasse: str):
    if klasse[0] == '0':
        return klasse, klasse[1:]
    if len(klasse) == 2:
        return '0' + klasse, klasse
    return (klasse,)

def standardklassenformat(klasse: str):
    if len(klasse) == 2:
        return '0' + klasse
    return klasse
stklform = standardklassenformat


def dictlistextend(dikt: dict, key: str, pluslist: set):
    '''Wird auf ein Dikt angewendet, in dem als Werte Listen gespeichert sind.
    Der Liste werden Werte hinzugefügt. Existiert der Schlüssel noch nicht,
    wird die Liste erstellt.'''
    if key in dikt:
        dikt[key].update(pluslist)
    else:
        dikt[key] = pluslist


def adressat(text: str) -> dict[str: list | bool]:
    '''Berechnet für die die Kategorien Klasse und Klassenstufe, an welche
    sich die Info richtet. AGs werden nur mit einem Wahrheitswert angegeben.'''
    ausgabe = dict()
    klasse = r'(?i)([01]?[0-9]) ?([a-h])(?!\w)'
    #spanne = lambda anfang, ende: fr'{anfang}( bis |( ?[-–] ?){ende}'
    klassenspanne = r'(?i)([01]?[0-9]) ?([a-h](?!\w))( bis | ?[-–] ?)[01]?[0-9]? ?([a-h])(?!\w)'
    if (erg := re.findall(klassenspanne, text)):
        erg = erg[0]
        stufe = erg[0]
        vonbuchst = erg[1]
        bisbuchst = erg[-1]
        b = 'abcdefgh'
        ausgabe['klasse'] = {stklform(stufe + i) for i in
                             b[b.index(vonbuchst):b.index(bisbuchst)+1]}

    if (erg := re.findall(klasse, text)):
        #print(erg)
        dictlistextend(ausgabe, 'klasse', {stklform(''.join(i)) for i in erg})

    stufenspanne = r'(?i)(?<=('
    #if (erg := regex.findall(stufenspanne, text)):
    #    pass

    stufennummer = r'[01]?[0-9](?!\w)'
    stufe1 = rf'(?i)(?<=(stufe|klassen?|jahrgang|jg\.?):? ){stufennummer}'
    stufe2 = r'[01]?[0-9](?=er|\. (Klasse|Stufe|Jahrgang))'
    
    if (erg := list(regex.finditer(rf'(?i)({stufe1})|({stufe2})', text))):
        ausgabe['stufe'] = {int(i.group(0)) for i in erg}

    stufenspanne = fr'(?P<start>(?<=(stufen?|klassen|jahrgang|jahrgänge|jg\.?) ){stufennummer})( bis | ?[-–] ?)(?P<stop>{stufennummer})'
    if (erg := regex.search(stufenspanne, text)):
        #try:
        if True:
            dictlistextend(ausgabe, 'stufe', set(range(int(erg.group('start')),
                                  int(erg.group('stop')) + 1)))
        #except IndexError:
        #    pass

    if 'AG' in text or 'GTA' in text:
        ausgabe['AG'] = True

    return ausgabe
    

def fürperson(person, text: str) -> bool | type('unbekannt'):
    '''Berechnet, ob eine Info für eine bestimmte Person relevant ist.
    Bei Gewissheit wird ein Wahrheitswert zurückgegeben, im Zweifels-
    fall "unbekannt".'''
    if person.vorname in text or person.nachname in text:
        return True
    adr = adressat(text)

    sm = adr.keys() & (pers := person.alsdict())
    if sm:
        return any(adr[schlüssel] & pers[schlüssel] for schlüssel in sm)

    return 'unbekannt'


def infosfürperson(person, text: str) -> list[str]:
    '''Bearbeitet einen Text zusätzlicher Informationen des Vertretungs-
    planes, indem er in einzelne Infos gegliedert wird, für die einzeln
    die Relevanz für die Person überprüft wird.'''
    if not text:
        return []
    
    erg = []
    abschnitte = [i.strip() for i in text.split('\n\n')]
    for i in abschnitte:
        if not (absatzfürpers := fürperson(person, i)):
            continue
        if absatzfürpers == 'unbekannt':
            erg.append(i)
            continue
        
        zeilen = [z.strip() for z in i.split('\n')]
        print(zeilen)
        if (anf := commonprefix(zeilen[1:])) and not zeilen[0].startswith(anf):
            print('ist eine Aufzählung')
            # Es handelt sich um eine Aufzählung.
            # Es sollte der Kopf gesendet werden und jene Zeilen, die
            # für die Person relevant sind.
            if fürperson(person, zeilen[0]) == True:
                erg.append(i)
            else:
                print('Nur einzelne Zeilen sind relevant.')
                erg.append('\n'.join([zeilen[0]] + [z for z in zeilen[1:]
                                                    if fürperson(person, z)]))
        else:
            erg.append(i)
    return erg


if __name__ == '__main__':
    from Vertretungsplanabrufen import Person
    hanno = Person('marlow.hanno@mcg-dresden.lernsax.de', ['9b'])
    t = '''Heute verkürzte Unterrichtszeiten (30-Minuten-Einheiten)!

Essenzeiten
nach 5. Stunde: 5b, 5d, 6c, 6d, 7c, 7d, 8a, 8b
nach 6. Stunde: 5a, 5c, 7a, 7b, 8c, 8b, 9b, 9c, 9d
nach 7. Stunde: 6a, 6b, 9a, 10a, 10b, 10c, 10d


Sonderplan Jahrgangsstufe 12:
- Leistungskurse planmäßig 3.-5. Stunde
- Grundkurse de/ma planmäßig 6./7. Stunde
- Grundkurse (P4/P5) It. Vertretungsplan (Rest entfällt)'''
    zeilen = t.split('\n\n')

    t2 = '''Mündlicher Abitur-Prüfungstag

Hausarbeitstag – Klassenstufe 7 bis 9
außer 7a Workshoptag,
7b, 7c Schulkonzert laut Vertretungsplan
9d – Exkursion mit Frau Steinbach
10. Klassen Profil laut Vertretungsplan

GTA Zaubern entfällt heute

Essenzeiten:
11:25 Uhr – 11:55 Uhr: alle Klassen 6 und Schüler Jg. 11 und 7b
12:20 Uhr: Klassen 5
Die zweite Essenpause entfällt.'''
