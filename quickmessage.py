import urllib.request as req
from urllib import response as res
from urllib.parse import urlparse
from bs4 import BeautifulSoup as BS


class Internetseite:
    def __init__(url, data=None, headers={}, bs=True):
        anfrage = req.Request(link, data=data, headers=headers)
        self.antwort = req.urlopen(url)
        self.html = self.antwort.read()

        if bs:
            self.bs = BS(s.read().decode(encoding=encoding), features="html.parser")


def bsseite(url, encoding='iso-8859-1'):
    global s
    s = req.urlopen(url)
    return BS(s.read().decode(encoding=encoding), features="html.parser")




def sendequickmessage(an, text, von, passwort):
    anmelden = bsseite('https://www.lernsax.de/wws/100001.php')

    addrneu = anmelden.find_all('form')[0]['action']

    # an die Seite 'https://www.lernsax.de' + addrneu soll nun eine Post-Anfrage mit meinen Anmeldedaten geschickt werden.

    with open('anmeldeformular.txt', 'r') as anfrage:
        anfrage = anfrage.read()

    addrneu = 'https://www.lernsax.de' + addrneu
    data = anfrage.format(passwort=passwort, email='marlow.nikolai@mcg-dresden.lernsax.de').encode()
    sid = urlparse(addrneu).query

    anmeldeversuch = req.Request(addrneu,
                                 data=data,
                                 headers = {'Content-Type': 'multipart/form-data; boundary=---------------------------251803576311549838173498707274',
                                            'Cookie': 'wwspc=1; wwsc=727532340210942241',})

    html = bsseite(anmeldeversuch)
    link = list(html.find(attrs={'id':'menu_105492'}).children)[0]['href']  # Adresse zum Messenger



    messenger = req.Request('https://www.lernsax.de/wws/' + link,
                         headers = {'Cookie': 'wwspc=1; wwsc=727532340210942241'})

    html = bsseite(messenger)
    # darin muss nach dem Bild der Sprechblase gesucht werden.
    #<img data-popup="106484.php?to=marlow.nikolai%40mcg-dresden.lernsax.de&amp;sid=74429484421984911845903054654614618767444829268754921370610921130890S5f5ef6fb" src="https://www.lernsax.de/pics/i_quick.svg" title="" class="set0 i1 mo oc" alt="Quickmessage schreiben" tabindex="0" data-html_tooltip="Quickmessage schreiben">
    #Der Link darin kann weiterverwendet werden.

    for quickmessagelink in html.findAll(name='img', attrs={'class': 'set0 i1 mo oc'}):
        if von.split('@')[0] in quickmessagelink['data-popup']:
            break
    quickmessagelink = quickmessagelink['data-popup']

    eingabefeld = req.Request('https://www.lernsax.de/wws/' + quickmessagelink,
                         headers = {'Cookie': 'wwspc=1; wwsc=727532340210942241'})

    html = bsseite(eingabefeld)

    empfänger_in = an if '@' not in an else an.split('@')[0]
    nachrichtentext = text

    with open('quickmessage.txt', 'r') as anfrage:
        data = anfrage.read().format(empfänger_in=empfänger_in, nachrichtentext=nachrichtentext)

    data = data.encode('iso-8859-1', 'xmlcharrefreplace')#'UTF-8')#'ansi')#encoding='iso-8859-1')
    referer = quickmessagelink
    addr = 'https://www.lernsax.de' + html.find(name='form')['action']
    quickmsg = req.Request(addr,
                         data=data,
                         headers = {'Content-Type': 'multipart/form-data; \
    boundary=---------------------------251803576311549838173498707274; charset=utf-8',
                                    'Cookie': 'wwspc=1; wwsc=727532340210942241',
                                    #'Content-Encoding': 'UTF-8'
                                    })

    q = req.urlopen(quickmsg)


if __name__ == '__main__':
    with open('passwort.txt', 'r') as pw:
        pw = pw.read()
