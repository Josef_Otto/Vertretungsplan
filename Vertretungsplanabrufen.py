import urllib.request as req
import urllib
from pprint import pprint
from bs4 import BeautifulSoup as BS
from itertools import zip_longest
import quickmessage
import os
import sys
import json
import datetime
import time
from enum import Enum
import re
from random import randint, random
import Zusatzinfos

os.chdir(os.path.dirname(os.path.abspath(__file__)))

def bsseite(url, encoding='UTF-8'):
    global s
    s = req.urlopen(url)
    return BS(s.read().decode(encoding=encoding), features="html.parser")

def trex(funkt, exc=ValueError, default=''):
    def fneu(*args, **kwargs):
        try:
            return funkt(*args, **kwargs)
        except exc:
            return default
    return fneu

def satzteil(text: str, wert, sonst=''):
    '''Satzteil'''
    if wert:
        return text.format(wert)
    return sonst.format(wert)
st = satzteil

def alsdatum(vpdatum):
    '''Berechnet ein datetime.date-Objekt aus der Zeitangabe im Vertretungsplan.
Diese hat die Form 'Montag, 25. März 2024 (B-Woche)'.'''
    _, tag, monat, jahr, *_ = vpdatum.split(' ')
    class Monate(Enum):
        Januar = 1
        Februar = 2
        März = 3
        April = 4
        Mai = 5
        Juni = 6
        Juli = 7
        August = 8
        September = 9
        Oktober = 10
        November = 11
        Dezember = 12
    return datetime.date(int(jahr), Monate[monat].value, int(float(tag)))  # Der Punkt hinterm Tag wird weggehackt.


with open('passwort.txt', 'r') as pw:
    pw = pw.read()


def vertretungsplanabrufen(bsmcgseite):
    mcgseite = bsmcgseite
    vpteil = mcgseite.find(name='div', attrs={'id': 'vplan-neu'})  # Es wird nur der neue Vertretungsplan betrachtet.
    #Auch der alte befindet sich noch im HTML.
    vertretungsplan = vpteil.find(name='table', attrs={'class':'unterricht'})
    
    zusatzinfos = vpteil.find(name='p', attrs={'class': 'fusszeile'})
    if zusatzinfos:
        zusatzinfos = ''.join(i if isinstance(i, str) else '\n' for i in
                              zusatzinfos.contents).strip()

    datum = vpteil.find(name='span', attrs={'class': 'vpfuer'}).contents[1].contents[0].strip()
    datum = ' '.join(datum.split(' ')[:-1])  # 'Montag, 22. April 2024 (A-Woche)'
        
    if not vertretungsplan:  # Gar kein VP
        vp = []
    else:
        vp = [[trex(j.contents.__getitem__, IndexError)(0) for j in i.findAll(name='td')] for i in vertretungsplan.findAll(name='tr')]
    return Vertretungsplan(vp, zusatzinfos=zusatzinfos, datum=datum)



class Vertretungsplan(list):
    def __init__(self, vpals2dliste, zusatzinfos=None, datum=None):
        super().__init__(vpals2dliste)
        [self.pop(i) for i, el in enumerate(self) if el == []]
        self.zusatzinfos=zusatzinfos
        self.datum = datum

    def zeilenüberklasse(self, klasse, regex=False):
        zeilenvon = self.zeilenvonklasse(klasse)
        if regex:
            return Vertretungsplan(i for i in self if re.search(klasse, ' '.join(i[1:])
                               and i not in zeilenvon))
        return Vertretungsplan(i for i in self if klasse in ' '.join(i[1:])
                               and i not in zeilenvon)
        

    def zeilenvonklasse(self, klasse, regex=False):
        if not regex:
            return Vertretungsplan(i for i in self if klasse == i[0])
        return Vertretungsplan(i for i in self if re.match(klasse, i[0]))

    def __str__(self):
        spaltenbreiten = [max(len(i) for i in spalte) for spalte in
                          zip_longest(*self.mitkopf())]
        #print(list(spaltenbreiten))
        return '\n'.join('|'.join(feld.ljust(breite) for breite, feld in
                                  zip(spaltenbreiten, zeile)) for
                                  zeile in self.mitkopf())

    def kurzfassung(self, fürklasse=''):
        '''Bringt den Vertretungsplan in Textform. Ist fürklasse vorgegeben,
        wird hervorgehoben, wenn die Änderung eigentlich für eine andere Klasse
        gilt oder z. B. nur eine Religruppe betrifft. '''
        
        def st(text: str, wert):
            '''Satzteil'''
            if wert:
                return text.format(wert)
            return ''
        
        erg = ''
        for klasse, stunde, fach, lehrer_in, raum, info in self:
            if klasse != fürklasse:
                    erg += f'Für {klasse}, '

            if fach != '—':
                satzteile = [st('St. {}:', stunde), st(' {}', fach), st(' bei {}', lehrer_in),
                         st(' in der {}', raum), st(' → {}', info)]
                erg += ''.join(satzteile)
            else:  # fällt aus.
                erg += f'St. {stunde}: {info}'
            
            erg += '\n'
        return erg.strip()

    def mitkopf(self):
        kopf = ['Klasse/Kurs', 'Stunde', 'Fach', 'Lehrer*in', 'Raum', 'Info']
        return [kopf] + self


class Person:
    def __init__(self, lsadresse, klassen, immersenden=False):
        self.adresse = lsadresse
        self.immersenden = immersenden
        self.klassen = [klassen] if isinstance(klassen, str) else klassen
        #self.klassen = ['0' + i if len(i) == 2 else i for i in self.klassen]#
        self.nachname, self.vorname = (i.capitalize() for i in
                                       lsadresse.split('@')[0].split('.'))
        hauptklasse = klassen[0] 
        self.klassenstufe = ''.join(i for i in hauptklasse if i.isdigit())  # löscht Buchstaben aus der Hauptklasse
        self.hauptklasse = hauptklasse
        self.oberstufe = int(self.klassenstufe) > 10
        #self.ermittleklassenstufe()#
        #if not self.oberstufe:#
        #    self.klassen.append(f'{self.klassenstufe}a-{self.klassenstufe}d')#

    '''def ermittleklassenstufe(self):
        for i in self.klassen:
            if (stufe := i[:2]).isdecimal():
                self.klassenstufe = stufe
                self.oberstufe = int(stufe) >= 11
                break
        #self.klassen.append(self.klassenstufe)
        if not self.oberstufe:
            self.hauptklasse = [i for i in self.klassen if len(i) == 3][0]
        else:
            if (e := next(i for i in self.klassen if len(i) == 3)):
                self.hauptklasse = e
                return
            self.hauptklasse = ', '.join(i for i in self.klassen if i[4].isupper())'''

    @classmethod
    def Unterstufe(cls, lsadresse, klasse: str, kurse: list = [], immersenden=False):
        '''Alternativer Konstruktor für Schüler*in aus der Unterstufe'''
        klassen = ['0' + klasse if len(klasse) == 2 else klasse] + kurse
        klassenstufe = self.hauptklasse[:-1]
        self = cls(lsadresse, klassen, immersenden)
        self.haupklasse = klasse
        self.klassen.append(f'{self.klassenstufe}a-{self.klassenstufe}d')
        self.oberstufe = False
        return self

    @classmethod
    def Oberstufe(cls, lsadresse, klasse, kurse: list, stufe, immersenden=False):
        '''Alternativer Konstruktor für Schüler*in aus der Oberstufe'''
        kurseohnestufe = kurse
        klassen = [f'{stufe}/ {i}' for i in kurse]
        self = cls(lsadresse, [klasse] + klassen, immersenden)
        self.LKs = [i for i in kurseohnestufe if i[0].isupper()]
        self.oberstufe = True
        self.hauptklasse = ''
        return self
    
    def änderungen(self, vp):
        meinvp = Vertretungsplan([])
        for i in self.klassen + [self.klassenstufe]:
            meinvp += vp.zeilenvonklasse(i)
        if self.klassenstufe[0] == '0':
            meinvp += vp.zeilenvonklasse(self.klassenstufe[1:])
        return meinvp

    def übrigens(self, vp):
        meinvp = Vertretungsplan([])
        for i in self.klassen:
            meinvp += vp.zeilenüberklasse(i)
        return meinvp

    def meinvptext(self, vp):
        erg = self.änderungen(vp).kurzfassung(self.hauptklasse)
        if not erg:  # Wenn es keine Änderungen unmittelbar für die Person
            # gibt, sind auch die übrigen Informationen nicht relevant...?
            return erg
        if (übrigens := self.übrigens(vp).kurzfassung()):
            erg += '\nÜbrigens:\n'
            erg += übrigens
        return erg

    def nachrichtentext(self, vp):
        satzteile = [
            st(f'''Dein Vertretungsplan für {vp.datum[:-5]}:

{{}}''', self.änderungen(vp).kurzfassung(self.hauptklasse),
               f'''Am {vp.datum[:-5]} gibt es für dich keine Änderungen.'''),
            st('\nÜbrigens:\n{}', self.übrigens(vp).kurzfassung()),
            st('\nZusätzliche Informationen:\n{}', self.zusatzinfos(vp.zusatzinfos)),
            st('\n\nWERBUNG:\n{}', str(WERBUNG))]

        return '\n'.join(i for i in satzteile if i)
        
        
    def vpanperson(self, vp):
        text = f'''Dein Vertretungsplan für {vp.datum}:

{self.meinvptext(vp)}'''
        quickmessage.sendequickmessage(self.adresse, text, 'marlow.nikolai@mcg-dresden.lernsax.de', pw) 
        print(f'Vertretungsplan an {self.vorname} {self.nachname} geschickt.')
        

    def vpanpersonwennnötig(self, vp, nurmorgen=True):
        if nurmorgen and alsdatum(vp.datum) - datetime.date.today() != datetime.timedelta(days=1):
            # Sendet den Vertretungsplan nur, wenn er für morgen oder heute ist.
            # Damit wird er nicht in den Ferien oder an Feiertagen verschickt.
            return

        if (not self.änderungen(vp) and not Zusatzinfos.infosfürperson(self, vp.zusatzinfos)) and not self.immersenden:
            return
        text = self.nachrichtentext(vp)
        
        quickmessage.sendequickmessage(self.adresse, text, 'marlow.nikolai@mcg-dresden.lernsax.de', pw)
        print(f'Vertretungsplan an {self.vorname} gesendet')

    def zusatzinfos(self, text):
        return '\n\n'.join(Zusatzinfos.infosfürperson(self, text))
        #return text

    def __str__(self):
        if not self.oberstufe:
            return f'{self.vorname} {self.nachname} aus der {self.hauptklasse}'
        return f'{self.vorname} {self.nachname}'

    def alsliste(self):
        return [self.adresse, self.klassen, self.immersenden]

    def alsdict(self):
        return {'klasse': set(self.klassen), 'stufe': {int(self.klassenstufe)},
                'AG': any(i.startswith('GTA') for i in self.klassen)}



class Werbung(str):
    def __new__(cls, werbedatei):
        datum, name, text = werbedatei.split('\n\n', 2)
        self = super().__new__(cls, text)
        self.datum = datum
        self.name = name
        return self

    def __str__(self):
        if datetime.datetime.fromisoformat(self.datum) >= datetime.datetime.now():
            return super().__str__()
        return ''


def vorbereiten():
    '''Gibt Werte für die Variablen vp, datum und WERBUNG zurück.'''
    for i in range(4):
        try:
            time.sleep(random())
            internetseite = bsseite('https://mcg-dresden.de/vertretung/')
        except urllib.error.URLError:
            print('Temporary failure in name resolution. Erneut versuchen...')
            time.sleep(randint(5, 20))
        else:
            break
    datum = internetseite.find(name='span', attrs={'class': 'vpfuer'}).contents[1].contents[0].strip()
    # datum hat das Format 'Montag, 25. März 2024 (B-Woche)'
    vp = vertretungsplanabrufen(internetseite)

    try:
        with open('Werbung.txt') as werbedatei:
            WERBUNG = Werbung(werbedatei.read())
    except FileNotFoundError:
        WERBUNG = ''

    return vp, datum, WERBUNG

if __name__ == '__main__':
    vp, datum, WERBUNG = vorbereiten()

    if len(sys.argv) > 1:
        #print('Huhu')
        daten = json.load(open(sys.argv[1]))
        person = Person(*daten)

        person.vpanpersonwennnötig(vp)


    else:
        nikolai = Person('marlow.nikolai@mcg-dresden.lernsax.de',
                         ['12F', '12/ ch2', '12/ PH1', '12/ ge4', '12/ MA3', '12/ fr1',
                          '12/ mu1', '12/ de3', '12/ geo1', '12/ inf2', '12/ spo5',
                          '12/ eth2', '12/ en21', 'GTA/ Chor', 'GTA/ MAspezi'],
                         immersenden=True)
        print(nikolai.nachrichtentext(vp))
